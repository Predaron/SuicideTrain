﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnitySampleAssets.Characters.ThirdPerson;

[RequireComponent(typeof(UnityEngine.AI.NavMeshAgent))]
[RequireComponent(typeof(ThirdPersonCharacter))]
public class AIControl : MonoBehaviour
{

    public UnityEngine.AI.NavMeshAgent agent { get; private set; } // the navmesh agent required for the path finding
    public ThirdPersonCharacter character { get; private set; } // the character we are controlling
    public Transform target; // target to aim for
    public float targetChangeTolerance = 1; // distance to target before target can be changed

    private Vector3 targetPos;

    public GameObject player;
    GameObject suicider;
    Vector3 startpos;
    Vector3 endpos;
    float fract;
    Animator animator;


    // Use this for initialization
    private void Start()
    {
        startpos = transform.position;
        endpos = transform.position + new Vector3(0.5f, 0, 0);
        player = GameObject.Find("Player");
        suicider = GameObject.Find("AICharacter");
        // get the components on the object we need ( should not be null due to require component so no need to check )
        agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
        character = GetComponent<ThirdPersonCharacter>();
        animator = GetComponent<Animator>();
        animator.SetBool("idletorun", false);
        animator.SetBool("runtoidle", true);
    }


    // Update is called once per frame
    private void Update()
    {
        if (transform.position.z - player.transform.position.z <= 5 && transform.position != endpos)
        {
            fract += 0.04f;
            transform.position = Vector3.Lerp(startpos, endpos, fract);
            animator.SetBool("idletorun", true);
            animator.SetBool("runtoidle", false);
            if ((transform.position.z - player.transform.position.z <= 2 && transform.position.z - player.transform.position.z >= -1) && (transform.position.x - player.transform.position.x <= 0.1 && transform.position.x - player.transform.position.x >= -0.1))
            {
                player.GetComponent<TrainController>().victims += 1;
                Destroy(this.gameObject);
            }
        }
        else
        {
            if ((transform.position.z - player.transform.position.z <= 2 && transform.position.z - player.transform.position.z >= -1) && (transform.position.x - player.transform.position.x <= 0.1 && transform.position.x - player.transform.position.x >= -0.1))
            {
                player.GetComponent<TrainController>().victims += 1;
                Destroy(this.gameObject);
            }
            animator.SetBool("idletorun", false);
            animator.SetBool("runtoidle", true);
        }
    }
}

