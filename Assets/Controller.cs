﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour {

	Vector3 pos;
    float maxSpeed = 0.2f; 
    float minSpeed = 0.05f; 
    float currentSpeed = 0.0000f;
    float acceleration = 0.0005f;
    float endTime;
    TextMesh textMesh;
    GameObject player;

    void Start()
    {
        player = GameObject.Find("Player");
        endTime = Time.time + 30;
        textMesh = GameObject.Find("Meshtext").GetComponent<TextMesh>();
        textMesh.text = "30";
    }

    void Update()
    {
        int timeLeft = (int)endTime - (int)Time.time;
        if (timeLeft < 0)
        {
            PlayerPrefs.SetFloat("Highscore", player.GetComponent<TrainController>().score);
            SceneManager.LoadScene(2);
            timeLeft = 0;
        }
        textMesh.text = timeLeft.ToString();
        if (Input.GetKey(KeyCode.S) && currentSpeed >= minSpeed)
        {
            transform.Translate(Vector3.forward * currentSpeed);
            currentSpeed -= acceleration;
            if (currentSpeed <= minSpeed)
            {
                currentSpeed = minSpeed;
            }
        }
        else {
            transform.Translate(Vector3.forward * currentSpeed);
            currentSpeed += acceleration;
            if (currentSpeed >= maxSpeed)
            {
                currentSpeed = maxSpeed;
            }
        }
    }
}
