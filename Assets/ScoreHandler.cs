﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScoreHandler : MonoBehaviour {

    public Text highscoretext;
    // Use this for initialization
	void Start () {
        highscoretext.text = "Your score : " + ((int)PlayerPrefs.GetFloat("Your score")).ToString();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
