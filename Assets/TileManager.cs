﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileManager : MonoBehaviour {

    public GameObject[] tilePrefabs;
    private Transform playerTransform;
    private float spawnZ = 12.6f;
    private float tileLength=4.2f;
    private int amnTilesOnScreen = 15;

	private void Start () {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        for (int i = 0; i < amnTilesOnScreen; i++) {
            SpawnTile();
        }
    }
	
	private void Update () {
		if(playerTransform.position.z>(spawnZ - amnTilesOnScreen * tileLength))
        {
            SpawnTile();
        }
	}

    private void SpawnTile(int prefabIndex = -1) {
        GameObject go;
        //Random rnd = new Random();
        go = Instantiate(tilePrefabs[Random.Range(0, 19)]) as GameObject;
        go.transform.SetParent(transform);
        go.transform.position = Vector3.forward * spawnZ;
        spawnZ += tileLength;
    }
}
