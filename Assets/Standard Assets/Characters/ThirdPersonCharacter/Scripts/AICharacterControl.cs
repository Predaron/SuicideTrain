using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace UnitySampleAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (UnityEngine.AI.NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
    public class AICharacterControl : MonoBehaviour
    {

        public UnityEngine.AI.NavMeshAgent agent { get; private set; } // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target; // target to aim for
        public float targetChangeTolerance = 1; // distance to target before target can be changed

        private Vector3 targetPos;

        GameObject player;
        GameObject suicider;
        Vector3 startpos;
        Vector3 endpos;
        float fract;
        Animator animator;

        // Use this for initialization
        private void Start()
        {
            startpos = transform.position;
            endpos = transform.position + new Vector3(0.5f, 0, 0);
            player = GameObject.Find("Player");
            suicider = GameObject.Find("AICharacter");

            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<UnityEngine.AI.NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();
            animator = GetComponent<Animator>();
            animator.SetBool("idletorun", false);
            animator.SetBool("runtoidle", true);
        }


        // Update is called once per frame
        private void Update()
        {
            /*if (transform.position.z - player.transform.position.z <= 5 && transform.position != endpos)
            {
                fract += 0.04f;
                //GetComponent<Rigidbody>().velocity = new Vector3(4, 0, 0);
                transform.position = Vector3.Lerp(startpos, endpos, fract);
                animator.SetBool("idletorun", true);
                animator.SetBool("runtoidle", false);
            }
            else {
                animator.SetBool("idletorun", false);
                animator.SetBool("runtoidle", true);
            }*/
            /*if (target != null)
            {
                // update the progress if the character has made it to the previous target
                if ((target.position - targetPos).magnitude > targetChangeTolerance)
                {
                    targetPos = target.position;
                    agent.SetDestination(targetPos);
                }

                // update the agents posiiton 
                agent.transform.position = transform.position;

                // use the values to move the character
                character.Move(agent.desiredVelocity, false, false, targetPos);
            }
            else
            {
                // We still need to call the character's move function, but we send zeroed input as the move param.
                character.Move(Vector3.zero, false, false, transform.position + transform.forward*100);

            }*/

        }

        /*public void SetTarget(Transform target)
        {
            this.target = target;
        }*/
    }

}
