================TE-109 (Soviet Locomotives)===============

Requires Unity 4.3.4 or higher.

This package includes model of Soviet locomotive TE-109 with 3 maps in 2 texture files:

- 2048x2048 - diffuse (RGB) + gloss (Alpha) in PSD format;

- 2048x2048 - normal (RGB) in PSD format.


Demo scene also included.
tris count of mesh: 3576 tris


Contact me: 
ilonionun@gmail.com


"IL.ranch", 2015.



