﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TrainController : MonoBehaviour
{
    Vector3 pos;
    GameObject player;
    GameObject camera;
    bool keyHit = false;
    bool rightEnabled = true;
    bool leftEnabled = true;
    float maxSpeed = 0.2f;
    float minSpeed = 0.05f;
    float currentSpeed = 0.0000f;
    float acceleration = 0.0005f;
    Collision col;
    GameObject suicider;
    public int score;
    public int victims=0;
    TextMesh textMesh;

    void Start()
    {
        player = GameObject.Find("Player");
        camera = GameObject.Find("CameraHolder");
        //suicider = GetComponent<CapsuleCollider>();
        textMesh = GameObject.Find("Victimtext").GetComponent<TextMesh>();
        textMesh.text = "Victims: 0";
    }

    void Update()
    {
        textMesh.text = "Victims: " + victims.ToString();
        pos = player.transform.position;
        score = (int)pos.z * 10 - victims * 100;
        if (Input.GetKey(KeyCode.S) && currentSpeed >= minSpeed)
        {
            transform.Translate(Vector3.up * currentSpeed);
            currentSpeed -= acceleration;
            if (currentSpeed <= minSpeed)
            {
                currentSpeed = minSpeed;
            }
        }
        else
        {
            transform.Translate(Vector3.up * currentSpeed);
            currentSpeed += acceleration;
            if (currentSpeed >= maxSpeed)
            {
                currentSpeed = maxSpeed;
            }
        }
        if (pos.x > -1.75f) {
            leftEnabled = true;
        }
        else
        {
            leftEnabled = false;
        }
        if (pos.x < 1.75f)
        {
            rightEnabled = true;
        }
        else
        {
            rightEnabled = false;
        }

        if ((pos.x >= -0.05f && pos.x <= 0.05f) || !leftEnabled || !rightEnabled)
        {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
            keyHit = false;
        }

        if (Input.GetKey(KeyCode.A) && leftEnabled && keyHit==false)
        {
            keyHit = true;
            GetComponent<Rigidbody>().velocity = new Vector3(-4, 0, 0);
        }
        
        if (Input.GetKey(KeyCode.D) && rightEnabled && keyHit==false)
        {
            keyHit = true;
            GetComponent<Rigidbody>().velocity = new Vector3(4, 0, 0);
        }
        OnCollisionEnter(col);
    }
    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.name == "SA_TrafficBarrier_01" || col.gameObject.name == "SA_OilDrum_02")
        {
            PlayerPrefs.SetFloat("Your score", score);
            Destroy(player);
            Destroy(camera);
            SceneManager.LoadScene(2);
        }
    }

    void OnGUI()
    {
        GUI.Label(new Rect(Screen.width - 200, Screen.height - 30, 400, 400), "Score: " + score.ToString());
    }
}
